package com.treslines.base;

public interface Restorable {

	/**
	 * Override this method to restore states from your app.</br>
	 * Is called when the app restarts or is called from the stack to the front
	 * again.
	 */
	void onRestoreCompleted();
	
}
