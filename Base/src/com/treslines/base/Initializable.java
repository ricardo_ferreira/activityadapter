package com.treslines.base;
/**
 * Initialization contract to be used in your Activity's super class abstraction.</br>
 * All methods from this initialization contract shall be called in the onCreate()</br> 
 * method from your Activitity's super class abstraction.
 * @author Ricardo Ferreira
 * @since 30/09/2014
 *
 */
public interface Initializable {

	/** Setup your activity's main layout in this method.</br>
	 * Here is the place to call setContentView(R.layout.yourLayout) */
	void onLayoutDefinition();

	/** Initialize your views in this method.</br>
	 * Here is the place to init your view components */
	void onViewInitialization();

	/** Create and add listeners to your views in this method.</br>
	 * Here is the place to add listeners to your initialized view components */
	void onViewListenerInitialization();
}
