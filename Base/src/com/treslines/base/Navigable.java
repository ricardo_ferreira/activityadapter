package com.treslines.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public interface Navigable {
	
	 /**
     * Use this method to navigate from one activity to another passing values to the target
     * activity
     */
    public void navigateToPassingValues(Context fromActivity, Class<?> toActivityClass,
            String bundleKey, Bundle bundle);

    /**
     * Use this method to navigate from one activity to another passing values to the target
     * activity
     */
    public void navigateToPassingValues(Context fromActivity, Class<?> toActivityClass, String key,
            String value) ;

    /** Use this method to navigate from one activity to another */
    public void navigateTo(Context fromActivity, Class<?> toActivityClass);

    /** Use this method to navigate directly to a given intent */
    public void navigateTo(Intent intent) ;
    
    /** Use this method to open the google play store from this app.</br>
     * Qualified app name example: "<b>com.yourCompany.YourAppName</b>" from your manifest file */
    public void navigateToGooglePlayStore(String qualifiedAppName) ;

    /** Use this method to open a specific web page.</br>
     * Address example: "<b>http://play.google.com/</b>" */
    public void navigateToWebpage(String address);

}
