package com.treslines.base;

import com.treslines.base.BaseActivity.VisibleListener;

public interface Visible {

	/**
	 * Override this method to be notified whenever the activity is visible to the user.</br>
	 *  That's the point where you have access to layout's height and width.</br>
	 * <b>IMPORTANT:</b> This method will only be called if the method onRootLayoutVisible() was overridden.
	 */
	void onActivityVisible();
	
	/** Override this method to enable onActivityVisible().</br>
	 * To implement it correctly, write:</br>
	 * <code>findViewById(R.id.rootLayout).getViewTreeObserver().addOnGlobalLayoutListener(l);</code>
	 * </br> and than override onActivityVisible() */
	void onRootLayoutVisible(VisibleListener l);
}
