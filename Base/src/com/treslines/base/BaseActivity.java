package com.treslines.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

/**
 * This base class provides a number of mechanisms to ensure the quality and</br>
 * stability of the applications. 
 * <pre>
 * To avoid "methods over flow" you should <b>NEVER</b> extend from it,
 * but furthermore from <b>BaseActivityAdapter</b>
 * </pre>
 * @author Ricardo Ferreira
 * @version 1.0
 * @since 29.08.2014
 * 
 */
public abstract class BaseActivity extends Activity implements Memorizable, Visible, 
Restorable, Initializable, Inevitable, Registerable, Rechargeable, Bootable, Depressible, Debuggable, Notifiable, Navigable{

	
	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		onLayoutDefinition();
		onRootLayoutVisible( new VisibleListener());		
		onViewInitialization();
		onViewListenerInitialization();
		onRegister();
	}
	
	@Override
	protected void onStop() {
		onSleepScreen();
		unregisterReceiver(this.BatteryLevelReceiver);
		unregisterReceiver(this.shutdownReceiver);
		unregisterReceiver(this.rebootReceiver);
		unregisterReceiver(this.incomingCallReceiver);
		unregisterReceiver(this.lowMemoryReceiver);
		unregisterReceiver(this.sdCardReceiver);
		super.onStop();
	}

	@Override
	public void onAttachedToWindow() {
		onActivityVisible();
		super.onAttachedToWindow();
	}


	@Override
	public void onBackPressed() {
		onBackKeyPressed();
		super.onBackPressed();
	}


	@Override
	protected void onDestroy() {
		onUnregister();
		super.onDestroy();
	}


	@Override
	protected void onPause() {
		super.onPause();
	}


	@Override
	protected void onResume() {
		super.onResume();
		onRestoreCompleted();
		registerReceiver(this.BatteryLevelReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		registerReceiver(this.shutdownReceiver, new IntentFilter(Intent.ACTION_SHUTDOWN));
		registerReceiver(this.rebootReceiver, new IntentFilter(Intent.ACTION_REBOOT));
		registerReceiver(this.incomingCallReceiver, new IntentFilter(Intent.ACTION_ANSWER));
		registerReceiver(this.lowMemoryReceiver, new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW));
		IntentFilter sdCardFilter = new IntentFilter(Intent.ACTION_MEDIA_MOUNTED);
		sdCardFilter.addDataScheme("file");
		registerReceiver(this.sdCardReceiver, sdCardFilter);
	}


	@Override
	protected void onUserLeaveHint() {
		onHomeKeyPressed();
		super.onUserLeaveHint();
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		loop: switch (keyCode) {
		case KeyEvent.KEYCODE_POWER:
			onPowerKeyPressed();
			break loop;
		case KeyEvent.KEYCODE_VOLUME_UP:
			onVolumeUpKeyPressed();
			break loop;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			onVolumeDownKeyPressed();
			break loop;
		}
		return super.onKeyDown(keyCode, event);
	}


	private BroadcastReceiver BatteryLevelReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int percentage = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 100);
			if (percentage < 6) {
				onLowBattery();
			}
		}
	};
	
	private BroadcastReceiver shutdownReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        onRebootRestartOrPowerOffIntent();
	    }
	};
	
	private BroadcastReceiver rebootReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	    	onRebootRestartOrPowerOffIntent();
	    }
	};
	
	private BroadcastReceiver incomingCallReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        onIncomingCall();
	    }
	};
	
	private BroadcastReceiver lowMemoryReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        onLowMemoryAvailable();
	    }
	};
	
	private BroadcastReceiver sdCardReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        onSdCardAvailable();
	    }
	};
	
	protected class VisibleListener implements OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
           onActivityVisible();
        }
    }

}
