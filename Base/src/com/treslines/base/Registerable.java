package com.treslines.base;

public interface Registerable {

	/** Override this method to register whatever you need.</br>
	 * Here is the place to register listeners, services, broadcast receivers etc.</br> 
	 * You may call for example: registerReceiver(); */
	void onRegister();
	/** Override this method to unregister whatever you have registered.</br>
	 * Here is the place to unregister listeners, services, broadcast receivers etc. */
	void onUnregister();
	
}
