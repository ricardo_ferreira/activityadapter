package com.treslines.base;

import android.content.Context;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

public class Notificator implements Notifiable {
	private Context mContext;
	private boolean mDebug;
	
	public Notificator(Context context,boolean... debug) {
		mContext=context;
		mDebug = debug[0];
	}
	
	@Override
	public void showShortMessage(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void showShortMessage(int id) {
		Toast.makeText(mContext, mContext.getString(id), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void showLongMessage(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
	}

	@Override
	public void showLongMessage(int id) {
		Toast.makeText(mContext, mContext.getString(id), Toast.LENGTH_LONG).show();
	}


	public void beep(final int howLongInMilliseconds) {
        try {
            final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION,
                    howLongInMilliseconds);
            tg.startTone(ToneGenerator.TONE_PROP_BEEP);
        } catch (Exception e) {//NOPMD
        	log(e.getMessage());
        }
    }

    public void soundNotification() {
        
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(mContext.getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {//NOPMD
        	log(e.getMessage());
        }

    }

    public void vibrate(final int howManyTimes) {
        try {
            final Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
            new Thread() {
                int start = 0;
                int count = howManyTimes;

                public void run() {
                    while (start < count) {
                        start++;
                        v.vibrate(1000);
                        try {
                            sleep(2000);
                        } catch (InterruptedException e) {
                        }
                    }
                };
            }.start();
        } catch (Exception e) {//NOPMD
        	log(e.getMessage());
        }
    }

    /**
	 * This methods logs only, if the method onDevelopment() returns true.</br> 
	 * To see the logs in the logcat view, define a filter with the tag <b>BASE<b/>
	 * @param logOutput the output you want to see in the logcat view
	 */
	protected void log(String logOutput){
		if(mDebug){Log.i("BASE", logOutput);}
	}
}
