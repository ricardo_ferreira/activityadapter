package com.treslines.base;

public interface Inevitable {
	
	/**
	 * Override this method to react to the sleep screen mode.</br>
	 * Is called when the sleep key button has been pressed. Use it to save
	 * states of your app.
	 */
	void onSleepScreen();

	
	/** Override this method to react to incoming calls.</br>
	 * Is called when a phone call comes in. Use it to save states of your app. */
	void onIncomingCall();

}
