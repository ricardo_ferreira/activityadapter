package com.treslines.base;

public interface Bootable {
	
	/**
	 * Override this method to react to the user's reboot, restart or power off intent.</br>
	 * Is called when the power off action has been performed. Use it to save states of your app.</br>
	 * <b>NOTE:</b> We are assuming that the user will do one of those actions described in</br>
	 * the method's signature. There is no guaranty that the user actually does it. 
	 */
	void onRebootRestartOrPowerOffIntent();
	
	/** Override this method to do whatever you may need when reboot is completed.</br>
	 * Is called when the device is restarted */
	void onRebootCompleted();

}
