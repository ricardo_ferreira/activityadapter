package com.treslines.base;


public class MainActivitySample extends BaseActivityAdapter {

	@Override
	public void onLayoutDefinition() {
		// uncomment this line bellow
		// setContentView(R.layout.yourLayout);
	}

	@Override
	public void onViewInitialization() {
		log("onViewInitialization");
		// you may initialize your view components and variables here...
	}

	@Override
	public void onViewListenerInitialization() {
		log("onViewListenerInitialization");
		// you may add listener to your components and variables here...
	}

	@Override
	public boolean onDevelopment() {
		// return false here as soon as you are done with the development
		return true;
	}

	

}
