package com.treslines.base;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class Navigator implements Navigable {
	
	private Context mContext;
	private boolean mDebug;
	
	public Navigator(Context context, boolean... debug) {
		mContext=context;
		mDebug = debug[0];
	}

    public void navigateToPassingValues(Context fromActivity, Class<?> toActivityClass,
            String bundleKey, Bundle bundle) {
        Intent activityToStart = createActivityToStart(fromActivity, toActivityClass);
        activityToStart.putExtra(bundleKey, bundle);
        mContext.startActivity(activityToStart);
    }

    public void navigateToPassingValues(Context fromActivity, Class<?> toActivityClass, String key,
            String value) {
        Intent activityToStart = createActivityToStart(fromActivity, toActivityClass);
        activityToStart.putExtra(key, value);
        mContext.startActivity(activityToStart);
    }

    public void navigateTo(Context fromActivity, Class<?> toActivityClass) {
        mContext.startActivity(createActivityToStart(fromActivity, toActivityClass));
    }

    public void navigateTo(Intent intent) {
    	mContext.startActivity(intent);
    }
    
    private Intent createActivityToStart(Context fromActivity, Class<?> toActivityClass) {
        return new Intent(fromActivity, toActivityClass);
    }

    
    public void navigateToGooglePlayStore(String qualifiedAppName) {
        final String appName = qualifiedAppName;
        try {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + appName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
        }
    }


    public void navigateToWebpage(String address) {
        final String webpage = address;
        try {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(webpage)));
        } catch (android.content.ActivityNotFoundException anfe) {// NOPMD
            // If anything goes wrong, don't disturb the user experience. just don't open the
            // webpage
        	log(anfe.getMessage());
        }
    }
    
    /**
   	 * This methods logs only, if the method onDevelopment() returns true.</br> 
   	 * To see the logs in the logcat view, define a filter with the tag <b>BASE<b/>
   	 * @param logOutput the output you want to see in the logcat view
   	 */
   	protected void log(String logOutput){
   		if(mDebug){Log.i("BASE", logOutput);}
   	}
    

}
