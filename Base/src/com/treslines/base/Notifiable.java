package com.treslines.base;

import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Vibrator;

public interface Notifiable {

	/**
	 * Use this method to show a short displayed message on the screen to the user
	 * @param msg the message to show
	 */
	public void showShortMessage(String msg);

	/**
	 * Use this method to show a short displayed message on the screen to the user
	 * @param id the resource id over R.string.yourId
	 */
	public void showShortMessage(int id);

	/**
	 * Use this method to show a long displayed message on the screen to the user
	 * @param msg the message to show
	 */
	public void showLongMessage(String msg);

	/**
	 * Use this method to show a long displayed message on the screen to the user
	 * @param id the resource id over R.string.yourId
	 */
	public void showLongMessage(int id);
	
	/**
	 * Use this method to notify the user with a simple beep
	 * @param howLongInMilliseconds how long the beep sound shall be
	 */
	public void beep(final int howLongInMilliseconds) ;
	/**
	 * Use this method to notify the user with the default system notification sound
	 */
    public void soundNotification() ;
    /**
     * Use this method to notify the user over vibration
     * @param howManyTimes how many times it should vibrate
     */
    public void vibrate(final int howManyTimes);

}
