package com.treslines.base;

public interface Debuggable {
	
	/** Override this method and return true, while developing it. Return false, when you are done!*/
	boolean onDevelopment();
}
