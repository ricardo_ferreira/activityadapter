package com.treslines.base;

public interface Memorizable {


	/** Override this method to react to sd card changes</br> 
	 * Will be called as soon as SDCards are inserted or available.</br>
	 * You may need to add this entry to your manifest file:<pre>
	 * 	< intent-filter >
     *  < action android:name="android.intent.action.MEDIA_MOUNTED" / >
     *  < data android:scheme="file" / > 
     *  < / intent-filter >
	 *  </pre>*/
	void onSdCardAvailable();
	
	/** Override this method to react to low memory.</br>
	 * Will be called as soon as low memory is available */
	void onLowMemoryAvailable() ;
}
