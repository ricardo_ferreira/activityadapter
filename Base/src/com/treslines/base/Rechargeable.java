package com.treslines.base;

public interface Rechargeable {

	/**
	 * Override this method to react to low battery level.</br>
	 * Will be called the first time, when the device has only 5% battery life.
	 * Here is the place to save something before the device is turning off
	 */
	void onLowBattery();
}
