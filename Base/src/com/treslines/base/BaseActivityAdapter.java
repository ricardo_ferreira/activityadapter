package com.treslines.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;


public abstract class BaseActivityAdapter extends BaseActivity {

	private Navigable mNavigator;
	private Notifiable mNotificator;
	
	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		mNavigator =  new Navigator(this, onDevelopment());
		mNotificator =  new Notificator(this, onDevelopment());
	}
	
	@Override
	public void onSleepScreen() {
		log("onSleepScreen:to be overridden as soon as needed");
	}

	@Override
	public void onBackKeyPressed() {
		log("onBackKeyPressed:to be overridden as soon as needed");
	}

	@Override
	public void onIncomingCall() {
		log("onIncomingCall:to be overridden as soon as needed");
	}

	@Override
	public void onHomeKeyPressed() {
		log("onHomeKeyPressed:to be overridden as soon as needed");
	}

	@Override
	public void onPowerKeyPressed() {
		log("onPowerKeyPressed:to be overridden as soon as needed");
	}

	@Override
	public void onVolumeUpKeyPressed() {
		log("onVolumeUpKeyPressed:to be overridden as soon as needed");
	}

	@Override
	public void onVolumeDownKeyPressed() {
		log("onVolumeDownKeyPressed:to be overridden as soon as needed");
	}

	@Override
	public void onLowBattery() {
		log("onLowBattery:to be overridden as soon as needed");
	}

	@Override
	public void onRegister() {
		log("onRegister:to be overridden as soon as needed");
	}

	@Override
	public void onUnregister() {
		log("onUnregister:to be overridden as soon as needed");
	}

	@Override
	public void onLowMemoryAvailable() {
		log("onLowMemoryAvailable:to be overridden as soon as needed");
	}

	@Override
	public void onRestoreCompleted() {
		log("onRestore:to be overridden as soon as needed");
	}

	@Override
	public void onSdCardAvailable() {
		log("onSdCardAvailable:to be overridden as soon as needed");
	}

	@Override
	public void onRebootRestartOrPowerOffIntent() {
		log("onRebootRestartOrPowerOffIntent:to be overridden as soon as needed");
	}

	@Override
	public void onRebootCompleted() {
		log("onRebootCompleted:to be overridden as soon as needed");
	}

	@Override
	public void onActivityVisible() {
		log("onActivityVisible:to be overridden as soon as needed");
	}

	@Override
	public void onRootLayoutVisible(VisibleListener l) {
		log("onRootLayoutVisible:to be overridden as soon as needed");
	}
	
	/**
	 * This methods logs only, if the method onDevelopment() returns true.</br> 
	 * To see the logs in the logcat view, define a filter with the tag <b>BASE<b/>
	 * @param logOutput the output you want to see in the logcat view
	 */
	public void log(String logOutput){
		if(onDevelopment()){Log.i("BASE", logOutput);}
	}
	
    public void beep(final int howLongInMilliseconds) {
        mNotificator.beep(howLongInMilliseconds);
    }

    public void soundNotification() {
        mNotificator.soundNotification();
    }

    public void vibrate(final int howManyTimes) {
       mNotificator.vibrate(howManyTimes);
    }
    
    @Override
	public void showShortMessage(String msg) {
    	mNotificator.showShortMessage(msg);
	}

	@Override
	public void showShortMessage(int id) {
		mNotificator.showShortMessage(id);
	}

	@Override
	public void showLongMessage(String msg) {
		mNotificator.showLongMessage(msg);
	}

	@Override
	public void showLongMessage(int id) {
		mNotificator.showLongMessage(id);
	}
	
    public void navigateToPassingValues(Context fromActivity, Class<?> toActivityClass,
            String bundleKey, Bundle bundle) {
    	mNavigator.navigateToPassingValues(fromActivity, toActivityClass, bundleKey, bundle);
    }

    public void navigateToPassingValues(Context fromActivity, Class<?> toActivityClass, String key,
            String value) {
    	mNavigator.navigateToPassingValues(fromActivity, toActivityClass, key, value);
    }

    public void navigateTo(Context fromActivity, Class<?> toActivityClass) {
    	mNavigator.navigateTo(fromActivity, toActivityClass);
    }

    public void navigateTo(Intent intent) {
    	mNavigator.navigateTo(intent);
    }
    
    public void navigateToGooglePlayStore(String qualifiedAppName){
    	mNavigator.navigateToGooglePlayStore(qualifiedAppName);
    }

    public void navigateToWebpage(String address){
    	mNavigator.navigateToWebpage(address);
    }
    
    public String[] getStringArrayFromStringXml(final int id) {
        return getResources().getStringArray(id);
    }

    /** Use this method to inflate layouts */
    public View inflateContentView(int layoutId) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(layoutId, null);
    }
    
    /** Use this method to get passed values over Intent.SetExtras(...) */
    public String getPassedUserInputSelection(String key) {
        return (String) getIntent().getExtras().get(key);
    }
    

	
}
