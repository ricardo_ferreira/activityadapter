package com.treslines.base;

public interface Depressible {
	
	
	/**
	 * Override this method to react to the back key.</br>
	 * Is called when the back key button is pressed. Use it to save states of your app.
	 */
	void onBackKeyPressed();
	/**
	 * Override this method to react to the home key</br>
	 * Is called when the home key button is pressed. Use it to save states of
	 * your app.
	 */
	void onHomeKeyPressed();
	
	/** Override this method to react to the power key</br>
	 * Is called when the sleep key button is long pressed. */
	void onPowerKeyPressed();

	/** Override this method to react to the volume up key</br>
	 * Is called when the volume up key button is pressed. */
	void onVolumeUpKeyPressed();

	/** Override this method to react to the volume down key</br>
	 * Is called when the volume down key button is pressed. */
	void onVolumeDownKeyPressed();

}
